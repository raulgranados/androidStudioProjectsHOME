package com.example.raulgranados.android2016;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends AppCompatActivity {

    TextView display_n;
    Button btn_1;
    Button btn_2;
    Button btn_3;
    Button btn_4;
    Button btn_5;
    Button btn_6;
    Button btn_7;
    Button btn_8;
    Button btn_9;
    Button btn_0;
    Double n1 = 0.0;
    Double n2 = 0.0;
    int operacion = 0;
    Button btn_suma;
    Button btn_resta;
    Button btn_multiplica;
    Button btn_igual;
    Button btn_dividir;
    Button btn_borrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        display_n = (TextView) findViewById(R.id.display_num);
        btn_1 = (Button) findViewById(R.id.boton1);
        btn_2 = (Button) findViewById(R.id.boton2);
        btn_3 = (Button) findViewById(R.id.boton3);
        btn_4 = (Button) findViewById(R.id.boton4);
        btn_5 = (Button) findViewById(R.id.boton5);
        btn_6 = (Button) findViewById(R.id.boton6);
        btn_7 = (Button) findViewById(R.id.boton7);
        btn_8 = (Button) findViewById(R.id.boton8);
        btn_9 = (Button) findViewById(R.id.boton9);
        btn_0 = (Button) findViewById(R.id.boton0);
        btn_suma = (Button) findViewById(R.id.boton_suma);
        btn_resta = (Button) findViewById(R.id.boton_resta);
        btn_multiplica = (Button) findViewById(R.id.boton_multiplica);
        btn_dividir = (Button) findViewById(R.id.boton_dividir);
        btn_igual = (Button) findViewById(R.id.boton_igual);
        btn_borrar = (Button) findViewById(R.id.boton_del);

        btn_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "1");
            }
        });

        btn_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "2");
            }
        });

        btn_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "3");
            }
        });

        btn_4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "4");
            }
        });

        btn_5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "5");
            }
        });

        btn_6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "6");
            }
        });

        btn_7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "7");
            }
        });


        btn_8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "8");
            }
        });

        btn_9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "9");
            }
        });


        btn_0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText(display_n.getText() + "0");
            }
        });

        btn_suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               n1 = Double.parseDouble(display_n.getText().toString());
               operacion = 1;
               display_n.setText("");
            }
        });

        btn_resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = Double.parseDouble(display_n.getText().toString());
                operacion = 2;
                display_n.setText("");
            }
        });

        btn_multiplica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 = Double.parseDouble(display_n.getText().toString());
                operacion = 3;
                display_n.setText("");
            }
        });


        btn_dividir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                n1 =  Double.parseDouble(display_n.getText().toString());
                operacion = 4;
                display_n.setText("");
            }
        });

        btn_borrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                display_n.setText("");
            }
        });


        btn_igual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (operacion == 1){
                    n2 =  Double.parseDouble(display_n.getText().toString());
                    double suma = n1 + n2;

                    display_n.setText(Double.toString(suma));
                }

                if (operacion == 2){
                    n2 =  Double.parseDouble(display_n.getText().toString());
                    double resta = n1 - n2;

                    display_n.setText(Double.toString(resta));
                }

                if (operacion == 3){
                    n2 =  Double.parseDouble(display_n.getText().toString());
                    double multiplica = n1 * n2;

                    display_n.setText(Double.toString(multiplica));
                }

                if (operacion == 4){
                    n2 =  Double.parseDouble(display_n.getText().toString());
                    Double division = n1/n2;

                    display_n.setText(Double.toString(division));
                }

            }
        });



    }

}
