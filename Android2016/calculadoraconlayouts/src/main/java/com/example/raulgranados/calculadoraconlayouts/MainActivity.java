package com.example.raulgranados.calculadoraconlayouts;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    TextView titulo;
    TextView etiqueta_op1;
    TextView etiqueta_op2;
    TextView etiqueta_resultado;

    EditText operando_1;
    EditText operando_2;
    EditText resultado;

    Button btn_suma;
    Button btn_resta;
    Button btn_multiplica;
    Button btn_divide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        titulo = (TextView)findViewById(R.id.label_titulo);

        etiqueta_op1 = (TextView)findViewById(R.id.label_operador_1);
        etiqueta_op2 = (TextView)findViewById(R.id.label_operador_2);
        etiqueta_resultado = (TextView)findViewById(R.id.label_resultado);

        operando_1 = (EditText)findViewById(R.id.display_operador1);
        operando_2 = (EditText)findViewById(R.id.display_operador2);
        resultado = (EditText)findViewById(R.id.display_resultado);

        btn_suma = (Button) findViewById(R.id.boton_suma);
        btn_resta = (Button) findViewById(R.id.boton_resta);
        btn_multiplica = (Button) findViewById(R.id.boton_multiplica);
        btn_divide = (Button) findViewById(R.id.boton_divide);

        btn_suma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (operando_1.getText().toString() != null && !operando_1.getText().toString().isEmpty() &&
                                    operando_2.getText().toString() != null && !operando_2.getText().toString().isEmpty()) {
                        double num1 = Double.parseDouble(operando_1.getText().toString());
                        double num2 = Double.parseDouble(operando_2.getText().toString());
                        double suma = num1 + num2;
                        resultado.setText(Double.toString(suma));
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR: alguno de los operandos esta sin informar", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e){
                    //nothing to do
                    Toast.makeText(getApplicationContext(), "ERROR: operación no permitida", Toast.LENGTH_LONG).show();
                }
            }
        });


        btn_resta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (operando_1.getText().toString() != null && !operando_1.getText().toString().isEmpty() &&
                            operando_2.getText().toString() != null && !operando_2.getText().toString().isEmpty()) {
                        double num1 = Double.parseDouble(operando_1.getText().toString());
                        double num2 = Double.parseDouble(operando_2.getText().toString());
                        double resta = num1 - num2;
                        resultado.setText(Double.toString(resta));
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR: alguno de los operandos esta sin informar", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e){
                    //nothing to do
                    Toast.makeText(getApplicationContext(), "ERROR: operación no permitida", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_multiplica.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (operando_1.getText().toString() != null && !operando_1.getText().toString().isEmpty() &&
                            operando_2.getText().toString() != null && !operando_2.getText().toString().isEmpty()) {
                        double num1 = Double.parseDouble(operando_1.getText().toString());
                        double num2 = Double.parseDouble(operando_2.getText().toString());
                        double multiplica = num1 * num2;
                        resultado.setText(Double.toString(multiplica));
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR: alguno de los operandos esta sin informar", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e){
                    //nothing to do
                    Toast.makeText(getApplicationContext(), "ERROR: operación no permitida", Toast.LENGTH_LONG).show();
                }
            }
        });

        btn_divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (operando_1.getText().toString() != null && !operando_1.getText().toString().isEmpty() &&
                            operando_2.getText().toString() != null && !operando_2.getText().toString().isEmpty()) {
                        double num1 = Double.parseDouble(operando_1.getText().toString());
                        double num2 = Double.parseDouble(operando_2.getText().toString());
                        double divide = num1 / num2;
                        resultado.setText(Double.toString(divide));
                    } else {
                        Toast.makeText(getApplicationContext(), "ERROR: alguno de los operandos esta sin informar", Toast.LENGTH_LONG).show();
                    }
                } catch (Exception e){
                    //nothing to do
                    Toast.makeText(getApplicationContext(), "ERROR: operación no permitida", Toast.LENGTH_LONG).show();
                }
            }
        });

    }
}

