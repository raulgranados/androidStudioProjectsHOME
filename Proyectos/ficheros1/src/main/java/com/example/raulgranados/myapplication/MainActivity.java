package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    private Button btnEscribirFichero = null;
    private Button btnLeerFichero = null;
    private Button btnLeerRaw = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEscribirFichero = (Button) findViewById(R.id.BtnEscribirFichero);

        btnLeerFichero = (Button) findViewById(R.id.BtnLeerFichero);

        btnLeerRaw = (Button) findViewById(R.id.BtnLeerRaw);

        //escribir en fichero
        btnEscribirFichero.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                {
                    try {

                        OutputStreamWriter fout = new OutputStreamWriter(openFileOutput("prueba_int.txt", Context.MODE_PRIVATE));

                        fout.write("Texto de prueba." + System.getProperty("line.separator") + "Con salto de linea");
                        fout.close();

                        Log.i("Ficheros", "Fichero creado!");

                    } catch (Exception ex) {

                        Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                    }
                }
            }
        });

        //leer de fichero
        btnLeerFichero.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    BufferedReader fin = new BufferedReader(new InputStreamReader(openFileInput("prueba_int.txt")));

                    String texto = fin.readLine();
                    fin.close();

                    Log.i("Ficheros", "Fichero leido!");
                    Log.i("Ficheros", "Texto: " + texto);
                } catch (IOException ex) {
                    Log.e("Ficheros", "Error al leer el fichero de memoria interna");
                }

            }
        });


        //leer de la carpeta de recursos
        //(previamente la hemos tenido que crear:
        //res -> new -> Android resources Directory -> Resource type -> raw )
        btnLeerRaw.setOnClickListener(new View.OnClickListener() {
        public void onClick (View v) {
            String linea = "";
            try {
                InputStream fraw =
                        getResources().openRawResource(R.raw.prueba_raw);

                BufferedReader brin =
                        new BufferedReader(new InputStreamReader(fraw));

                linea = brin.readLine();
                fraw.close();

                Log.i("Ficheros", "Fichero RAW leido!");
                Log.i("Ficheros", "Texto: " + linea);
            } catch (Exception ex) {
                Log.e("Ficheros", "Error al leer fichero desde recurso raw");
            }
        }
        });
    }
}
