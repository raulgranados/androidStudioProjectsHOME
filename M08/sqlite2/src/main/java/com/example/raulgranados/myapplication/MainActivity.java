package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

/**
 * Created by raulgranados on 21/2/16.
 */
public class MainActivity extends Activity {
    TextView textoTitulo;
    TextView textoResultado;
    Button btnConsultar;
    Button btnModificar;

    static SQLiteDatabase db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textoTitulo = (TextView) findViewById(R.id.texto_titulo_app);

        textoResultado = (TextView) findViewById(R.id.texto_resultado_app);
        //String titulo = texto.getText().toString();

        btnConsultar = (Button) findViewById(R.id.btn_mostrar_canciones);
        btnModificar = (Button) findViewById(R.id.btn_añadir_canciones);


        UsuariosSQLHelper usdbh = new UsuariosSQLHelper(this, "musica.db", null, 1);

        db = usdbh.getWritableDatabase();

        usdbh.onCreate(db);

        //db.execSQL("DROP TABLE IF EXISTS africa" );
        db.execSQL("INSERT INTO africa (titulo, autor, pais, año) VALUES ('My people' ,'Youssou N Dour', 'Senegal', 1994)");
        db.execSQL("INSERT INTO africa (titulo, autor, pais, año) VALUES ( 'Vuka Vuka', 'The Manhattan Brothers', 'South Africa', 1954)");
        db.execSQL("INSERT INTO africa (titulo, autor, pais, año) VALUES ( 'Mother of Hope', 'Ladysmith Black Mambazo', 'South Africa', 1973)");
        db.execSQL("INSERT INTO africa (titulo, autor, pais, año) VALUES ('Desert roots', 'Hamid Baroudi', 'Argelia', 1994)");


        btnConsultar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Cursor c = db.rawQuery("SELECT * FROM " + MusicContract.FeedEntry.NOMBRE_TABLA, null);


                String from[] = new String[]{MusicContract.FeedEntry.NOMBRE_COLUMNA_TITULO, MusicContract.FeedEntry.NOMBRE_COLUMNA_AUTOR};
                int to[] = new int[]{R.id.text1, R.id.text2};

                SimpleCursorAdapter dataAdapter = new SimpleCursorAdapter(MainActivity.this, R.layout.row, c, from, to, 0);

                ListView lv = (ListView) findViewById(R.id.android_list);
                lv.setAdapter(dataAdapter);
            }
        });

       btnModificar.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               // creamos el Intent
               Intent intent = new Intent(MainActivity.this, ModificarActivity.class);
               /*
               //creamos la informacion a pasar
               Bundle b = new Bundle();
               b.putString("NOMBRE", txtNombre.getText().toString());

               // añadimos la informacion al intent
               intent.putExtras(b);
               */
               // Iniciamos la nueva actividad (pasamos por parametro el intent)
               startActivity(intent);


           }
       });
    }
}

