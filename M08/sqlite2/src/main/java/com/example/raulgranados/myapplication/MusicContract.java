package com.example.raulgranados.myapplication;

import android.provider.BaseColumns;

/**
 * Created by raulgranados on 21/2/16.
 */
public class MusicContract {

    public MusicContract(){}

    public static abstract class FeedEntry implements BaseColumns {

        public static final String NOMBRE_TABLA = "africa";
        public static final String NOMBRE_COLUMNA_ID = "_id";
        public static final String NOMBRE_COLUMNA_TITULO = "titulo";
        public static final String NOMBRE_COLUMNA_AUTOR = "autor";
        public static final String NOMBRE_COLUMNA_PAIS = "pais";
        public static final String NOMBRE_COLUMNA_AÑO = "año";

    }

}
