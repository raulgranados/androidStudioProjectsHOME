package org.escoladeltreball.iam47870239.listviewpelicules;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Exercici6Activity extends AppCompatActivity implements OnItemClickListener {

    private String[] pelicules;
    private String[] anys;

    TextView textView;
    ArrayAdapter<Movie> adapter;
    ListView listView;
    EditText editText;
    List<Movie> sortedMoviesList;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercici4);

        textView = (TextView) findViewById(R.id.top_movies);

        // LListat de les 100 películes
        // pelicules = getResources().getStringArray(R.array.movie_title);
        // anys = getResources().getStringArray(R.array.movie_year);

        // Llistat de les 1000 películes
        pelicules = getResources().getStringArray(R.array.movie_title2);
        anys = getResources().getStringArray(R.array.movie_year2);

        // Iniciem l'ArrayList on desarem objectes de tipus Movie amb les dades corresponents
        sortedMoviesList = new ArrayList<Movie>();

        for (int i = 0; i < pelicules.length; i++) {
            sortedMoviesList.add(new Movie(i + 1, pelicules[i], anys[i]));
        }

        // Iniciem l'adapter personalitzat i  el setejem al ListView
        adapter = new MovieAdapter(sortedMoviesList, this);

        listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);

        // Jo he utilitzat un edit text per tal de filtrar les películes
        // El que farem és crear un nou adapter cada cop que modifiquem l'EditText
        // amb els elements que comencin per la sequència de caràcters uqe contingui l'EditText
        editText = (EditText) findViewById(R.id.search_movie);
        editText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int textLength = editText.getText().length();
                sortedMoviesList.clear();

                // Recorrem el llistat de películes i agafem solament les que comencin per la sequència definida el l'EditText
                for (int i = 0; i < pelicules.length; i++) {
                    if (textLength <= pelicules[i].length()) {
                        if (editText.getText().toString().equalsIgnoreCase((String) pelicules[i].subSequence(0, textLength))) {
                            sortedMoviesList.add(new Movie(i + 1, pelicules[i], anys[i]));
                        }
                    }
                }

                adapter = new MovieAdapter(sortedMoviesList, Exercici6Activity.this);
                listView.setAdapter(adapter);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercici4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        textView.setText(sortedMoviesList.get(position).getName());
    }
}
