package org.escoladeltreball.iam47870239.listviewpelicules;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by jduran on 2/1/16.
 */
public class MovieAdapter extends ArrayAdapter<Movie> {

    private List<Movie> movieList;
    private Context context;

    public MovieAdapter(List<Movie> movieList, Context context) {
        super(context, R.layout.movie_item, movieList);
        this.movieList = movieList;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        // Primer comprovem que el convertView no sigui null, en cas contrari, imflem el layout
        if (convertView == null) {
            // Aquesta es la nova view que inflem amb el nou layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_item, parent, false);
        }
        // Omplim el layout amb els valors corresponents
        TextView tvNumber = (TextView) convertView.findViewById(R.id.number);
        TextView tvName = (TextView) convertView.findViewById(R.id.name);
        TextView tvYear = (TextView) convertView.findViewById(R.id.year);

        String number = movieList.get(position).getNumber() + "";
        String name = movieList.get(position).getName();
        String year = movieList.get(position).getYear();

        tvNumber.setText(number);
        tvName.setText(name);
        tvYear.setText(year);

        return convertView;
    }

}
