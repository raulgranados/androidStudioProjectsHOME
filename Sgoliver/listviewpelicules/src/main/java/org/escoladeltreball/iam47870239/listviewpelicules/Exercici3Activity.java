package org.escoladeltreball.iam47870239.listviewpelicules;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class Exercici3Activity extends AppCompatActivity implements OnItemClickListener {

    private String[] movies;

    private ListView listViewMovies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercici123);

        // Array amb el nom de les 100 pelícues
        movies = getResources().getStringArray(R.array.movie_title);

        listViewMovies = (ListView) findViewById(R.id.list);
        // Creem l'adaptador del listView
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.movie_simple_item, R.id.movie_name, movies);

        // Setejem el filtrat de text
        listViewMovies.setTextFilterEnabled(true);
        // Setegen aquest adapter al listView
        listViewMovies.setAdapter(adapter);
        // Li posem a la llista un escoltador
        listViewMovies.setOnItemClickListener(this);

        Toast.makeText(this, getResources().getText(R.string.advertencia), Toast.LENGTH_SHORT).show();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercici1, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String info = new String();
        info += "PARENT = " + parent;
        info += "\nVIEW = " + view;
        info += "\nVIEW'S TEXT = " + movies[position];
        info += "\nPOSITION = " + position;
        info += "\nID = " + id;

        Toast.makeText(this, info, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // *** Extra ***
        // Definim el que ha de fer quan es premi la tecla de baixar el volum
        if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN) {
            // Mostrar teclat
            InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            imm.showSoftInput(listViewMovies, InputMethodManager.SHOW_IMPLICIT);
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
