package org.escoladeltreball.iam47870239.listviewpelicules;

/**
 * Created by jordidn on 2/1/16.
 */
public class Movie {

    private int number;
    private String name;
    private String year;

    public Movie(int number, String name, String year) {
        this.number = number;
        this.name = name;
        this.year = year;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
