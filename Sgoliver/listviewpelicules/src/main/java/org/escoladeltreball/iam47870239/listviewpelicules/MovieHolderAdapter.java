package org.escoladeltreball.iam47870239.listviewpelicules;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by iam47870239 on 1/13/16.
 */
public class MovieHolderAdapter extends ArrayAdapter<Movie> {
    private List<Movie> movieList;
    private Context context;

    public MovieHolderAdapter(List<Movie> movieList, Context context) {
        super(context, R.layout.movie_item, movieList);
        this.movieList = movieList;
        this.context = context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolderItem viewHolder;

        // Primer comprovem que el convertView no sigui null, en cas contrari, imflem el layout
        if (convertView == null) {
            // Aquesta es la nova view que inflem amb el nou layout
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.movie_item, parent, false);

            viewHolder = new ViewHolderItem();
            viewHolder.tvNumber = (TextView) convertView.findViewById(R.id.number);
            viewHolder.tvTitle = (TextView) convertView.findViewById(R.id.name);
            viewHolder.tvYear = (TextView) convertView.findViewById(R.id.year);
            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolderItem) convertView.getTag();
        }

        Movie movieItem = movieList.get(position);

        if (movieItem != null) {
            String number = movieList.get(position).getNumber() + "";
            String name = movieList.get(position).getName();
            String year = movieList.get(position).getYear();

            viewHolder.tvNumber.setText(number);
            viewHolder.tvTitle.setText(name);
            viewHolder.tvYear.setText(year);
        }

        return convertView;
    }

    static class ViewHolderItem {
        TextView tvNumber;
        TextView tvTitle;
        TextView tvYear;
    }
}
