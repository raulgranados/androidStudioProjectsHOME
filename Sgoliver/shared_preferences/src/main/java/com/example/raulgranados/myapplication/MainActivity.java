package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {

    private Button btnGuardar;
    private Button btnCargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btnGuardar = (Button) findViewById(R.id.BtnGuardar);
        btnCargar = (Button) findViewById(R.id.BtnCargar);

        btnGuardar.setOnClickListener(new View.OnClickListener() {

                                          @Override
                                          public void onClick(View v) {
                                              //guardamos las preferencias
                                              //obtener una referencia a una coleccion determinada
                                              SharedPreferences prefs = getSharedPreferences("Mis Preferencias", Context.MODE_PRIVATE);

                                              //actualizar-insertar nuevas preferencias
                                              SharedPreferences.Editor editor = prefs.edit();

                                              editor.putString("email", "modificado@email.com");
                                              editor.putString("nombre", "prueba");

                                              editor.commit();

                                          }
                                      }
        );

        btnCargar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    //Recuperamos las preferencias
                    SharedPreferences prefs =
                            getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

                    String correo = prefs.getString("email", "por_defecto@email.com");
                    String nombre = prefs.getString("nombre", "nombre_por_defecto");
                    String otra = prefs.getString("otra", "otra_por_defecto");

                    Log.i("Preferences", "Correo: " + correo);
                    Log.i("Preferences", "Nombre: " + nombre);
                    Log.i("Preferences", "Otra: " + otra);

                }
        });

    }

    /**
     * Created by raulgranados on 9/3/16.
     */
    public static class MiBaseDatos extends SQLiteOpenHelper {
        private static final int VERSION_BASEDATOS = 1;// la versión de nuestra base de datos

        // Nombre de nuestro archivo de base de datos
        private static final String NOMBRE_BASEDATOS = "mibasedatos.db"; //sera el nombre de nuestro archivo de base de datos

        // Sentencia SQL para la creación de una tabla
        private static final String TABLA_CONTACTOS = "CREATE TABLE contactos" +
                "(_id INT PRIMARY KEY, nombre TEXT, telefono INT, email TEXT)";


        // CONSTRUCTOR de la clase
        public MiBaseDatos(Context context) {
            super(context, NOMBRE_BASEDATOS, null, VERSION_BASEDATOS);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(TABLA_CONTACTOS);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLA_CONTACTOS);
            onCreate(db);
        }
    }
}

