package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

public class MainActivity extends Activity {

    private Button btnSinHilos;
    private Button btnHilo;
    private Button btnAsyncTask;
    private Button btnCancelar;

    private ProgressBar pbarProgreso;

    private MiTareaAsincrona tarea1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnSinHilos = (Button) findViewById(R.id.btnSinHilos);
        btnHilo = (Button) findViewById(R.id.btnHilo);
        btnAsyncTask = (Button)findViewById(R.id.btnAsyncTask);
        btnCancelar = (Button)findViewById(R.id.btnCancelar);

        pbarProgreso = (ProgressBar) findViewById(R.id.pbarProgreso);

        // 1) REALIZAMOS LA TAREA LARGA EN EL HILO PRINCIPAL:
        // ATENCION: la app se bloquea y no se puede ver el progresso de la barra ( entre otras cosas-fallos);
        btnSinHilos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pbarProgreso.setMax(100); //valor máximo que alcanzará
                pbarProgreso.setProgress(0); //inicialización del valor mínimo a 0

                for (int i = 1; i <= 10; i++) {
                    tareaLarga();
                    pbarProgreso.incrementProgressBy(10);
                }

                Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
            }
        });

    // 2) CREAMOS UN HILO SECUNDARIO PARA REALIZAR LA TAREA LARGA:
    // ATENCION: hay que tener en cuenta que en los hilos secundarios no podemos hacer referencia
    // directa a componentes que se ejecuten en el hilo principal, entre ellos los controles
    // que forman nuestra interfaz de usuario, es decir, que desde el método run()
    // no podríamos ir actualizando directamente nuestra barra de progreso de la misma forma que lo hacíamos antes.

    btnHilo.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick (View v){

            new Thread(new Runnable() {

                public void run() {

                    //SOLUCIÓN 1: utilizamos el método post para actuar sobre los controles de la interfaz.
                    pbarProgreso.post(new Runnable() {
                        public void run() {
                            pbarProgreso.setProgress(0);
                        }
                    });

                    for (int i = 1; i <= 10; i++) {

                        tareaLarga();

                        pbarProgreso.post(new Runnable() {
                            public void run() {
                                pbarProgreso.incrementProgressBy(10);
                            }
                        });
                    }

                    // SOLUCIÓN 2: llamamos al método runOnUiThread() para enviar operaciones al hilo principal.
                    runOnUiThread(new Runnable() {
                        public void run() {
                            Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
            }).start();
        }
    });

     // 3) ASYNCTASK
        btnAsyncTask.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                tarea1 = new MiTareaAsincrona();
                tarea1.execute();
            }

        });

        btnCancelar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                tarea1.cancel(true);
            }
        });


}
    private void tareaLarga(){
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // 3) CREAMOS UNA CLASE ASYNCTASK
    // doInBackground no recibira ningun parámetro de entrada -> void,
    // el tipo de datos con el que actualizaremos el progreso de la tarea
    // (que recibira el método onProgressUpdate y que tb pasaremos cómo parámetro al
    // método publishProgress() -> int
    // El tipo de datos que devolveremos como resultado de nuestra tarea, que será el tipo
    // de retorno del método doInBackground() y el tipo del parámetro recibido en el método
    // onPostExecute() -> boolean.
    private class MiTareaAsincrona extends AsyncTask<Void, Integer, Boolean> {

        @Override
        protected Boolean doInBackground(Void... params) {

            for(int i=1; i<=10; i++) {
                tareaLarga();

                publishProgress(i*10);

                //hemos introducido es la posibilidad de cancelar la tarea en
                // medio de su ejecución.
                if(isCancelled())
                    break;
            }

            return true;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            int progreso = values[0].intValue();

            pbarProgreso.setProgress(progreso);
        }

        @Override
        protected void onPreExecute() {
            pbarProgreso.setMax(100);
            pbarProgreso.setProgress(0);
        }

        @Override
        protected void onPostExecute(Boolean result) {
            if(result)
                Toast.makeText(MainActivity.this, "Tarea finalizada!", Toast.LENGTH_SHORT).show();
        }

        @Override
        protected void onCancelled() {
            Toast.makeText(MainActivity.this, "Tarea cancelada!", Toast.LENGTH_SHORT).show();
        }
    }


}
