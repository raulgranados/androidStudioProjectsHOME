package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


    public class MainActivity extends Activity {


        private TextView lblEtiqueta;
        private ListView lstOpciones;

        private Titular[] datos =
                new Titular[]{
                        new Titular("Título 1", "Subtítulo largo 1"),
                        new Titular("Título 2", "Subtítulo largo 2"),
                        new Titular("Título 3", "Subtítulo largo 3"),
                        new Titular("Título 4", "Subtítulo largo 4"),
                        new Titular("Título 5", "Subtítulo largo 5"),
                };

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);


            lblEtiqueta = (TextView) findViewById(R.id.LblEtiqueta);
            lstOpciones = (ListView) findViewById(R.id.LstOpciones);

            //Cabecera
            View header = getLayoutInflater().inflate(R.layout.list_header, null);
            lstOpciones.addHeaderView(header);

            //Adaptador
            AdaptadorTitulares adaptador =
                    new AdaptadorTitulares(this, datos);

            lstOpciones.setAdapter(adaptador);

            //Eventos
            lstOpciones.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> a, View v, int position, long id) {

                    //Alternativa 1:
                    String opcionSeleccionada =
                            ((Titular) a.getItemAtPosition(position)).getTitulo();

                    //Alternativa 2:
                    //String opcionSeleccionada =
                    //		((TextView)v.findViewById(R.id.LblTitulo))
                    //			.getText().toString();

                    lblEtiqueta.setText("Opción seleccionada: " + opcionSeleccionada);
                }
            });
        }

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }

        //3)
        class ViewHolder {
            TextView titulo;
            TextView subtitulo;
        }
        //reutilizar layouts a traves del parametro convertView
        class AdaptadorTitulares extends ArrayAdapter<Titular> {

            public AdaptadorTitulares(Context context, Titular[] datos) {
                super(context, R.layout.listitemtiular, datos);
            }

            //reutilizar layouts a traves del parametro convertView
            public View getView(int position, View convertView, ViewGroup parent) {
                //LayoutInflater inflater = LayoutInflater.from(getContext());
                //View item = inflater.inflate(R.layout.listitemtiular, null);

                //1)
                View item = convertView;

                //4)
                ViewHolder holder;

                //2)
                if (item == null) {
                    LayoutInflater inflater = LayoutInflater.from(getContext());
                    item = inflater.inflate(R.layout.listitemtiular, null);

                    //5)
                    holder = new ViewHolder();
                    holder.titulo = (TextView)item.findViewById(R.id.LblTitulo);
                    holder.subtitulo = (TextView)item.findViewById(R.id.LblSubTitulo);

                    item.setTag(holder);
                }
                else
                {
                    holder = (ViewHolder)item.getTag();
                }

                holder.titulo.setText(datos[position].getTitulo());
                holder.subtitulo.setText(datos[position].getSubtitulo());

                return(item);
            }
        }
    }

