package com.example.raulgranados.sgoliver;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {
    private EditText txtNombre;
    private Button btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // obtenemos la referencia de los diferentes views a manipular

        txtNombre = (EditText) findViewById(R.id.TxtNombre);
        btnAceptar = (Button) findViewById(R.id.BtnAceptar);

        // implementamos el evento click del boton
        btnAceptar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                // creamos el Intent
                Intent intent = new Intent(MainActivity.this, SaludoActivity.class);

                //creamos la informacion a pasar
                Bundle b = new Bundle();
                b.putString("NOMBRE", txtNombre.getText().toString());

                // añadimos la informacion al intent
                intent.putExtras(b);

                // Iniciamos la nueva actividad (pasamos por parametro el intent)
                startActivity(intent);


            }
        });


    }
}

