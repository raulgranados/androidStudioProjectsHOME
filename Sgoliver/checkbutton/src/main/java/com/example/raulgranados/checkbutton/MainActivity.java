package com.example.raulgranados.checkbutton;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final CheckBox cbMarcame = (CheckBox)findViewById(R.id.checkboxMarcame);

        cbMarcame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isChecked = ((CheckBox)v).isChecked();

                if (isChecked) {
                    cbMarcame.setText("Checkbox marcado");
                } else {
                    cbMarcame.setText("Checkbox desmarcado");
                }
            }
        });

    }

}
