package com.example.raulgranados.myapplication;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

/**
 * Created by raulgranados on 9/3/16.
 */
public class MiContentProvider  extends ContentProvider {

    //variables de la clase

    //Primeramente creamos una instancia a nuestra base de datos y un objeto "SQLiteDataBase"
    // que sera el encargado de hacer las consultas CRUD (crear, leer, actualizar, borrar)
    // en nuestra base de datos.
    private MiBaseDatos MBD;
    SQLiteDatabase SQLDB;

    //Continuamos creando el identificador de nuestro Content Provider "NOMBRE_CP"
    private static final String NOMBRE_CP = "mi.content.provider.contactos";

    //
    private static final int CONTACTOS = 1;
    private static final int CONTACTOS_ID = 2;
    private static final UriMatcher uriMatcher =
            new UriMatcher(UriMatcher.NO_MATCH);

    static{
        uriMatcher.addURI(NOMBRE_CP, "contactos", CONTACTOS);
        uriMatcher.addURI(NOMBRE_CP, "contactos/#", CONTACTOS_ID);
    }


    //llamado al iniciar el Content Provider (Aquí simplemente iniciamos nuestra base de datos.)
    @Override
    public boolean onCreate() {
        MBD = new MiBaseDatos(getContext());
        return true;
    }

    //devuelve los datos a la persona que los consulte
    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        //En este caso ponemos nuestra base de datos en modo lectura y cargamos las consultas
        // en un cursor ya que este método devuelve un cursor.

        Cursor c = null;
        try {
            switch (uriMatcher.match(uri)) {
                case CONTACTOS_ID:
                    String id = "_id=" + uri.getLastPathSegment();
                    SQLDB = MBD.getReadableDatabase();
                    c = SQLDB.query("contactos", projection, id, selectionArgs,
                            null, null, null, sortOrder);
                    break;
                case CONTACTOS:
                    SQLDB = MBD.getReadableDatabase();
                    c = SQLDB.query("contactos", projection,   null, selectionArgs,
                            null, null, null, sortOrder);
                    break;
            }
        } catch (IllegalArgumentException e) {
            Log.e("ERROR", "Argumento no admitido: " + e.toString());
        }

        return c;
    }

    //devuelve el tipo MIME de los datos del Content Provider (MIME se podría definir como una serie de convenciones
    // o especificaciones dirigidas al intercambio de todo tipo de archivos (texto, audio, vídeo, ...)
    // a través de Internet de una forma transparente para el usuario)
    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    //inserta nuevos datos en el Content Provider
    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        long registro = 0;//la usaremos para comprobar si se inserto bien el registro

        //Encapsulamos todo en un bloque try-catch para tratar la excepción "IllegalArgumentExcepcion"
        // que se produce cuando se llama a un método con un argumento que no es razonable de tratar.
        // Dentro comprobamos el argumento "uri" y si es igual a nuestra uriMatcher "CONTACTOS"
        // ponemos nuestra base de datos en modo escritura e insertamos el registro en nuestra tabla
        // "contactos" usando el argumento values.

        //El método insert devuelve la uri del nuevo registro.
        try {
            if (uriMatcher.match(uri) == CONTACTOS) {
                SQLDB = MBD.getWritableDatabase();
                registro = SQLDB.insert("contactos", null, values);
            }
        } catch (IllegalArgumentException e) {
            Log.e("ERROR", "Argumento no admitido: " + e.toString());
        }

        // Comprobar si se inserto bien el registro
        if (registro > 0) {
            Log.e("INSERT", "Registro creado correctamente");
        } else {
            Log.e("Error", "Al insertar registro: " + registro);
        }

        return Uri.parse("contactos/" + registro);
    }

    //borra datos del Content Provider
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {

        //Aquí mas de lo mismo, comprobamos la uri y si es igual almacenamos la id del
        // registro y lo borramos con el método delete.

        // Este método nos devolverá la id del registro borrado.

        int registro = 0;
        try {
            if (uriMatcher.match(uri) == CONTACTOS_ID) {
                String id = "_id=" + uri.getLastPathSegment();
                SQLDB = MBD.getWritableDatabase();
                registro = SQLDB.delete("contactos", id, null);
            }
        } catch (IllegalArgumentException e) {
            Log.e("ERROR", "Argumento no admitido: " + e.toString());
        }

        return registro;
    }

    //actualiza datos existentes en el Content Provider
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {

        //en este caso comprobamos que la uri sea igual a nuestra usriMatcher "CONTACTOS_ID".
        // En caso de que sea así almacenamos el registro a actualizar en la variable "id"
        // y para ello usamos el método "getLastPathSegment()" que nos devuelve el ultimo segmento
        // de la uri decodificado.

        //Este método nos devolverá la id que hemos actualizado.

        String id = "";
        try {
            if (uriMatcher.match(uri) == CONTACTOS_ID) {
                id = uri.getLastPathSegment();
                SQLDB = MBD.getWritableDatabase();
                SQLDB.update("contactos", values, "_id=" + id, selectionArgs);
            }
        } catch (IllegalArgumentException e) {
            Log.e("ERROR", "Argumento no admitido: " + e.toString());
        }

        return Integer.parseInt(id);

    }
}

