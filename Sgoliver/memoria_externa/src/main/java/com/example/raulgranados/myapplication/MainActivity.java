package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boolean sdDisponible = false;

        boolean sdAccesoEscritura = false;

        String estado = Environment.getExternalStorageState();

        if (estado.equals(Environment.MEDIA_MOUNTED))

        {

            sdDisponible = true;

            sdAccesoEscritura = true;

        }

        else if (estado.equals(Environment.MEDIA_MOUNTED_READ_ONLY))

        {

            sdDisponible = true;

            sdAccesoEscritura = false;

        }

        else

        {

            sdDisponible = false;

            sdAccesoEscritura = false;

        }

        //Si la memoria externa está disponible y se puede escribir
        if (sdDisponible && sdAccesoEscritura) {


            try

            {

                //File ruta_sd = Environment.getExternalStorageDirectory();

                File ruta_sd = getExternalFilesDir(null);

                File f = new File(ruta_sd.getAbsolutePath(), "prueba_sd.txt");

                OutputStreamWriter fout =

                        new OutputStreamWriter(

                                new FileOutputStream(f));

                fout.write("Texto de prueba.");

                fout.close();

            } catch (Exception ex)

            {

                Log.e("Ficheros", "Error al escribir fichero a tarjeta SD");

            }

            try

            {

                //File ruta_sd = Environment.getExternalStorageDirectory();

                File ruta_sd = getExternalFilesDir(null);

                File f = new File(ruta_sd.getAbsolutePath(), "prueba_sd.txt");

                BufferedReader fin =

                        new BufferedReader(

                                new InputStreamReader(

                                        new FileInputStream(f)));

                String texto = fin.readLine();

                fin.close();

                Log.e("Ficheros", "El texto es: "+ texto);

            }

            catch (Exception ex)

            {

                Log.e("Ficheros", "Error al leer fichero desde tarjeta SD");

            }
        }
    }
}
