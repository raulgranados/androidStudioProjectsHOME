package org.escoladeltreball.iam38561924.contentprovider;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ShowInListContacts extends Activity {
    Cursor phones;
    ArrayList<MiContacto> contactos;
    ListView lista;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_in_list_contacts);

        Intent intent = getIntent();
        String search = intent.getStringExtra("contacto");

        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        contactos = (ArrayList<MiContacto>) ShowChosenContact.buscarContacto(search, phones);
        if (contactos.size() > 0) {


        lista = (ListView) findViewById(R.id.listViewContactes);
        ArrayAdapter<MiContacto> arrayAdapter = new ArrayAdapter<MiContacto>(
                this,
                android.R.layout.simple_list_item_1,
                contactos);
        lista.setAdapter(arrayAdapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            Intent intent;
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MiContacto miContacto=(MiContacto)lista.getItemAtPosition(position);
                intent = new Intent(ShowInListContacts.this, InfoGenericaActivity.class);
                intent.putExtra("infoGenerica",miContacto.datosCompletos());
                startActivity(intent);
            }
        });
        }else{

            this.finish();
            Toast.makeText(this, "No hubieron resultados en la busqueda", Toast.LENGTH_LONG).show();
        }
    }

}
