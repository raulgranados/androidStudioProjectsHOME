package org.escoladeltreball.iam38561924.contentprovider;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by iam38561924 on 2/29/16.
 */
public class MiContacto {


    HashMap<String, String> contactData;

    public MiContacto(HashMap<String, String> contactData) {
        this.contactData = contactData;
    }

    @Override
    public String toString() {
        return contactData.get("display_name") + System.getProperty("line.separator") + contactData.get("contact_id");
    }

    public String datosCompletos() {

        String data = "";
        Iterator it = contactData.entrySet().iterator();

        while (it.hasNext()) {
            Map.Entry e = (Map.Entry) it.next();
            data += e.getKey() + ":" + e.getValue() + System.getProperty("line.separator");
        }


        return data;
    }
}
