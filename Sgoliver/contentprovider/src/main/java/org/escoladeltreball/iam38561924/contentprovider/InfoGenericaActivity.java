package org.escoladeltreball.iam38561924.contentprovider;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

public class InfoGenericaActivity extends Activity {
TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_generica);
        Intent intent=getIntent();
        textView = (TextView)findViewById(R.id.infoGenerica);
        textView.setText(intent.getCharSequenceExtra("infoGenerica"));
    }

}
