package org.escoladeltreball.iam38561924.contentprovider;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.pm.LabeledIntent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.MimeTypeMap;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

import java.io.PrintWriter;

public class ShowContacts extends Activity {


    ListView lista;
    TextView textView;
    String infoContactos;
    String listacontactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_contacts);
        getIntent();

        textView = (TextView) findViewById(R.id.infoCampos);
        ContentResolver cr = getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
        infoContactos = "";
        listacontactos = "";
        infoContactos += "URI:" + ContactsContract.Contacts.CONTENT_URI + System.getProperty("line.separator") + System.getProperty("line.separator");

        infoContactos += "Tipus MIME:" + ContactsContract.Contacts.CONTENT_TYPE + System.getProperty("line.separator") + System.getProperty("line.separator");
        for (int i = 0; i < cur.getColumnCount(); i++) {
            infoContactos += i + ":" + cur.getColumnName(i) + System.getProperty("line.separator");

        }


        infoContactos += "" + System.getProperty("line.separator");


        Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);

        while (phones.moveToNext()) {
            String name = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
            String phoneNumber = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
            String id = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone._ID));
           // System.out.println(".................." + phoneNumber);
            listacontactos += "Id:" + id + ", Name:" + name + System.getProperty("line.separator");
        }

        phones.close();

        infoContactos += listacontactos;
        textView.setText(infoContactos);

    }


}
