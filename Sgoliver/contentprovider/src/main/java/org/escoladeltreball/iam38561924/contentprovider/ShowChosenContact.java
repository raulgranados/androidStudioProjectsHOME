package org.escoladeltreball.iam38561924.contentprovider;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ShowChosenContact extends Activity {
    TextView muestraContacto;
    String infoContactos = "";
    Cursor phones;
    ArrayList<MiContacto> contactos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_chosen_contact);
        Intent intent = getIntent();
        String search = intent.getStringExtra("contacto");
        muestraContacto = (TextView) findViewById(R.id.contacto);
        phones = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, null, null, null);
        contactos = (ArrayList<MiContacto>) buscarContacto(search, phones);
        int i = 0;
        if (contactos.size() > 0) {
            for (MiContacto mc : contactos) {
                infoContactos += "CONTACTO:" + i + System.getProperty("line.separator") + mc.datosCompletos() + System.getProperty("line.separator") + System.getProperty("line.separator");
                i++;
            }
            muestraContacto.setText(infoContactos);


        } else {
            this.finish();
            Toast.makeText(this, "No hubieron resultados en la busqueda", Toast.LENGTH_LONG).show();
            //  muestraContacto.setText("No hubieron resultados");
        }

    }


    public static List<MiContacto> buscarContacto(String string, Cursor phones) {
        ArrayList<MiContacto> result = new ArrayList<>();
        HashMap<String, String> misContactos;


        while (phones.moveToNext()) {

            if ((phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))).contains(string)) {
                // Log.e("lista contactos",(phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))) );

                misContactos = new HashMap<>();
                for (int j = 0; j < phones.getColumnCount(); j++) {
                    misContactos.put(phones.getColumnNames()[j], phones.getString(j));


                }

                result.add(new MiContacto(misContactos));


            }
        }
        //  Log.e("resul", result.toString());

        return result;
    }
}
