package org.escoladeltreball.iam38561924.contentprovider;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CursorAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Switch;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends Activity implements View.OnClickListener {
    Button mostrar;
    Button infoContacto;
    Button listContacto;
    EditText buscarContacto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mostrar = (Button) findViewById(R.id.mostar);
        infoContacto = (Button) findViewById(R.id.infoContacto);
        listContacto = (Button) findViewById(R.id.listContacto);
        buscarContacto = (EditText) findViewById(R.id.buscar);
        mostrar.setOnClickListener(this);
        infoContacto.setOnClickListener(this);
        listContacto.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.mostar:
                intent = new Intent(this, ShowContacts.class);
                startActivity(intent);
                break;
            case R.id.infoContacto:
                if (!buscarContacto.getText().toString().isEmpty()) {
                    intent = new Intent(this, ShowChosenContact.class);
                    intent.putExtra("contacto", buscarContacto.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "introduce parte del contacto a buscar", Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.listContacto:
                if (!buscarContacto.getText().toString().isEmpty()) {
                    intent = new Intent(this, ShowInListContacts.class);
                    intent.putExtra("contacto", buscarContacto.getText().toString());
                    startActivity(intent);
                } else {
                    Toast.makeText(this, "introduce parte del contacto a buscar", Toast.LENGTH_SHORT).show();
                }
                break;
        }


    }
}
