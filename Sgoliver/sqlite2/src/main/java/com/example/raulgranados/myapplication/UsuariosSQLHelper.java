package com.example.raulgranados.myapplication;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

public class UsuariosSQLHelper extends SQLiteOpenHelper {

    //Sentencia SQL para crear la tabla Africa
    //String africa = "CREATE TABLE IF NOT EXISTS africa( _id INTEGER PRIMARY KEY AUTOINCREMENT, titulo TEXT, autor TEXT, pais TEXT, any INTEGER )";

    private static final String SQL_CREATE_AFRICA = "CREATE TABLE IF NOT EXISTS "
            + MusicContract.FeedEntry.NOMBRE_TABLA
            + "("
            + MusicContract.FeedEntry.NOMBRE_COLUMNA_ID + " INTEGER PRIMARY KEY,"
            + MusicContract.FeedEntry.NOMBRE_COLUMNA_TITULO + " TEXT,"
            + MusicContract.FeedEntry.NOMBRE_COLUMNA_AUTOR + " TEXT,"
            + MusicContract.FeedEntry.NOMBRE_COLUMNA_PAIS + " TEXT,"
            + MusicContract.FeedEntry.NOMBRE_COLUMNA_AÑO + " INTEGER"
            + ")";

    //Sentencia SQL para eliminar la tabla Africa
    private static final String SQL_DELETE_AFRICA = "DROP TABLE IF EXISTS " + MusicContract.FeedEntry.NOMBRE_TABLA;


    public UsuariosSQLHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        //Se ejecuta la sentencia SQL de creacion de la tabla Africa
        db.execSQL(SQL_CREATE_AFRICA);
        //db.execSQL(africa);
        Log.i("", "la tabla "+ MusicContract.FeedEntry.NOMBRE_TABLA + " ha sido creada");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Por simplicidad del ejemplo aquí utilizamos directamente la opción de
        // eliminar la tabla anterior y crearla de nuevo vacía con el nuevo formato.

        //Se elimina la versión anterior de la tabla
        db.execSQL(SQL_DELETE_AFRICA);

        //Se crea la nueva versión de la tabla || onCreate(db)
        db.execSQL(SQL_CREATE_AFRICA);
        //db.execSQL(africa);

    }
}
