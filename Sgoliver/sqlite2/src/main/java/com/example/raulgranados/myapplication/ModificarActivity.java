package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by raulgranados on 21/2/16.
 */
public class ModificarActivity extends Activity {
    EditText textoTitulo;
    EditText textoAutor;
    EditText textoPais;
    EditText textoAño;
    Button btnInsertat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modificacion);

        textoTitulo = (EditText)findViewById(R.id.textotitulo);
        final String titulo = textoTitulo.getText().toString();
        textoAutor = (EditText)findViewById(R.id.textoautor);
        final String autor = textoAutor.getText().toString();
        textoPais = (EditText)findViewById(R.id.textopais);
        final String pais = textoPais.getText().toString();
        textoAño = (EditText)findViewById(R.id.textoaño);

        final int año = Integer.parseInt(textoAño.getText().toString());
        Log.i("","el año es: " + año);
        btnInsertat = (Button)findViewById(R.id.btn_añadir_canciones);

        btnInsertat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity.db.execSQL("INSERT INTO africa (titulo, autor, pais, año) VALUES ( " + titulo + ", " + autor + ", " + pais + ", " + año +")");
            }
        });
    }



}
