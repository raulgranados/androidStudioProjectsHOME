package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
//ok
public class MainActivity extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView texto = (TextView) findViewById(R.id.contactos);

        Cursor c = managedQuery(ContactsContract.Contacts.CONTENT_URI,
                new String[] {ContactsContract.Contacts.DISPLAY_NAME},
                ContactsContract.Contacts.IN_VISIBLE_GROUP,
                null,
                ContactsContract.Contacts.DISPLAY_NAME);

        while (c.moveToNext()) {
            String contactos = c.getString(c
                    .getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
            texto.append(contactos);
            texto.append("\n");
        }
    }
}
