package com.example.raulgranados.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class MainActivity extends Activity {
    String texto;
    String linea;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Ejemplo de escritura en un fichero de memoria externa
        try {
            OutputStreamWriter fout = new OutputStreamWriter( openFileOutput("prueba.txt", Context.MODE_PRIVATE));

            fout.write("texto de prueba.");

            fout.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            Log.e("Ficheros", "Error al escribir");
        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Ficheros", "Error al escribir");
        }

        //Ejemplo de lectura desde un fichero de memoria interna

        try {
        BufferedReader fin = new BufferedReader( new InputStreamReader( openFileInput("prueba.txt")));

            texto = fin.readLine();
            fin.close();

        } catch (IOException e) {
            e.printStackTrace();
            Log.e("Ficheros", "Error al leer");
        }

        Toast.makeText(this, "el texto es: "+texto, Toast.LENGTH_LONG).show();

        //Ejemplo de acceso a un fichero de la carpeta res.raw
        try
        {
            InputStream fraw =
                    getResources().openRawResource(R.raw.prueba);
            BufferedReader brin =
                    new BufferedReader(new InputStreamReader(fraw));
                   // linea = brin.readLine();
                    while ((linea=brin.readLine())!=null){
                        Log.e("Ficheros", "El texto es: " + linea);
        }
            fraw.close();
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde recurso raw");
        }

        //Log.e("Ficheros", "El texto es: " + linea);
    }
}
